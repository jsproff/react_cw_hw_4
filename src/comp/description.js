import React from 'react';

const Desc = ({ children }) => {
  return(
    <div>
      <h3>Album name: {children.name}</h3>
      <p className="desc">
        {children.about}
      </p>
      <br/>
      <h4>Songs:</h4>
      <ul>
        {
          children.compositions.map(
            (song, index) => {
              return <li key={index}>
                name: {song.name}
                <br/>
                duration: {song.duration}
              </li>
            }
          )
        }
      </ul>
    </div>
  )
};

export default Desc;