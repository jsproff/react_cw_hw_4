import React from 'react';
import { Link} from 'react-router-dom';
import Desc from './description'

const Album = ({albums, match}) => {
  const backId = match.params.id;
  return(
    <div>
      <Link to={'/album/' + backId}>Back</Link>
      <h2>Description</h2>
      <Desc>
        {albums}
      </Desc>
    </div>
  )
}

export default Album;