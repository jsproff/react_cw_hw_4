import React from 'react';

import { Link} from 'react-router-dom';

const Albums = ({artist, match}) => {
  const {album, name} = artist;
  let totalDuration = 0;
  return(
    <div className="home">
      <Link to="/">Back</Link>
      <h1>Artist: {name}</h1>
      <h2>Albums</h2>
      <br/>
      {
        album.map((item, index) => {
          return <div key={index}>
            <Link to={'/album/' + match.params.id + '/' + index}>{item.name}</Link>
            <br/>
            {
              item.compositions.map(
                (song, index) => {
                  index === 0 ? totalDuration = 0 : '';
                  totalDuration += parseFloat(song.duration.replace(':', '.'))
                }
              )
            }
            <span>Total duration: {totalDuration. toFixed(2)}</span>
          </div>
        })
      }
    </div>
  )
}

export default Albums;