import React from 'react';
import { Link } from 'react-router-dom';

const Home = ({ data }) => {
  return(
    <div className="home">
      <h1>home</h1>
      {
        data.map((item, index) => {
          return item.album.length !== 0 ? <div key={index}><Link to={'/album/' + item.index}>{item.name}</Link></div> : <span key={index}>{item.name}</span>
        })
      }
    </div>
  )
};

export default Home;