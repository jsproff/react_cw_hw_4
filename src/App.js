import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import './App.css';
import Home from './comp/home'
import Albums from './comp/albums'
import Album from './comp/album'
import NotFound from "./comp/404";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoaded: false
    }
  }
  componentDidMount() {
    const url = 'http://www.json-generator.com/api/json/get/bGyYXYiCOG?indent=2';
    fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({
          data: data,
          isLoaded: true
        });
      })
  }
  render() {
    if(this.state.isLoaded) {
      return (
        <BrowserRouter basename="/">
          <Switch>
            <Route path="/" exact render={() => <Home data={this.state.data}/>}/>
            <Route path="/album/:id" exact render={({match}) => <Albums artist={this.state.data[match.params.id]} match={match} />} />
            <Route path="/album/:id/:ida" render={({match}) => <Album albums={this.state.data[match.params.id].album[match.params.ida]} artist={this.state.data[match.params.id].name} match={match} />}/>
            <Route component={NotFound}/>
          </Switch>
        </BrowserRouter>
      )
    } else {
      return null
    }
  }
};

export default App;
